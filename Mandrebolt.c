#include <stdio.h>
#include <mpi.h>

const double minR = -2.0;
const double maxR = 1.0;
const double minI = -1.0;
const double maxI = 1.0;
const int Iteracion = 500;
const int m_widht = 400, m_height = 400;

double convertir_real(int x , int y, int width)
{
    double Real = minR + (x * (maxR - minR) / width);
    return Real;
}

double convertir_imaginario(int x, int y, int height)
{
    double imaginario = minI + (y * (maxI - minI) / height);
    return imaginario;
}

int mandelbrot(double r, double i, int iteracion_max)
{
    double zr = 0.0f, zi = 0.0f;
    int j = 0;
    for (j = 0; j < iteracion_max && zr * zr + zi * zi < 4.0; j++)
    {
        double temp = zr * zr - zi * zi + r;
        zi = 2.0 * zr * zi + i;
        zr = temp;
    }

    return j;
}

int main(int argc, char** argv)
{
    FILE *salida = fopen("imagen.ppm", "w");
    fprintf(salida, "P3\n");
    fprintf(salida, "%d %d\n", m_widht, m_height);
    fprintf(salida, "255\n");
    int color[m_widht];
    int node, numProcesos, row;
    int Count = 0;

    MPI_Init(&argc,&argv);
    MPI_Comm_rank(MPI_COMM_WORLD, &node);
    MPI_Comm_size(MPI_COMM_WORLD, &numProcesos);
    MPI_Status status, status2, status3, status4;
    double inicio, fin;
    int inc = (m_height / (numProcesos - 1))+1;

    if(node == 0)
    {
        inicio = MPI_Wtime();
        for (int i = 0, row = 0; i < numProcesos - 1; i++, row = row + inc)
        {
            MPI_Send(&row, 1, MPI_INT, i + 1, i + 1, MPI_COMM_WORLD);
            MPI_Send(&Count, 1, MPI_INT, i + 1, i + 1, MPI_COMM_WORLD);
            MPI_Send(&inc, 1, MPI_INT, i + 1, i + 1, MPI_COMM_WORLD);
            Count = Count + inc;

        }
        for (int i = 0; i <  m_height; i++)
        {
            MPI_Recv(color, m_widht, MPI_INT, MPI_ANY_SOURCE, i, MPI_COMM_WORLD, &status2);
          
            for (int x = 0; x < m_widht; x++)
            {
                int rr = (color[x] % 255);
                int g = (color[x] % 255);
                int b = (color[x] % 255);
  
                fprintf(salida, "%d %d %d\n", rr, g, b);
            }
        }
        fin = MPI_Wtime();
        printf("El tiempo fue de: %f\n", (fin-inicio));
    }
    else
    {
        MPI_Recv(&row, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status);
        MPI_Recv(&Count, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status3);
        MPI_Recv(&inc, 1, MPI_INT, 0, MPI_ANY_TAG, MPI_COMM_WORLD, &status4);

        for (int y = Count; y < row + inc; y++)
        {
            for (int x = 0; x < m_widht; x++)
            {
                double r = convertir_real(x, y, m_widht);
                double i = convertir_imaginario(x, y, m_height);

                color[x] = mandelbrot(r, i, Iteracion);
            }
            MPI_Send(color, m_widht, MPI_INT, 0, y, MPI_COMM_WORLD);
        }
    }
  
    MPI_Finalize();
    fflush(salida);
    fclose(salida);
    return 0;
}
